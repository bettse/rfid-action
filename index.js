/**
 * @format
 */
const fs = require('fs');
const {spawn} = require('child_process');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const shell = require('default-shell');

const {DEBUG, SERIAL_PORT} = process.env;
const START_OF_TEXT = '\u0002';
const END_OF_TEXT = '\u0003';

if (!SERIAL_PORT) {
  console.log('SERIAL_PORT not defined');
  process.exit(1);
}

const port = new SerialPort(SERIAL_PORT, {baudRate: 9600});
const parser = port.pipe(new Readline({delimiter: `\r\n${END_OF_TEXT}`}));

port.on('error', err => {
  console.error(err);
});

port.on('open', () => {
  console.log('ready');
});

parser.on('data', data => {
  if (data.charAt(0) === START_OF_TEXT) {
    data = data.substring(1);
  }

  // Reload config file each time incase it changed
  const configFile = fs.readFileSync('./config.json');
  const config = JSON.parse(configFile);

  try {
    const tag = verifyChecksum(Buffer.from(data, 'hex'));
    const envCommand = process.env[tag] || process.env[tag.toUpperCase()];
    const configCommand = config[tag] || config[tag.toUpperCase()];
    const command = envCommand || configCommand;
    // TODO: Support object commands which specify working directory, shell, etc
    // TODO: Support base64 encoded object commands for adding to process.env
    if (command) {
      console.log(`> ${tag} => ${command}`);
      const stdio = DEBUG ? 'inherit' : 'ignore';
      const options = {
        // cwd: User's home
        shell,
        detached: true,
        stdio,
      };
      const process = spawn(command, [], options);
      process.on('error', console.error);
    } else {
      console.log(`> no command for ${tag}`);
    }
  } catch (e) {
    console.log(e);
    return;
  }
});

/**
 * @param {Buffer} data 5 bytes of tag + 1 check byte
 * @return {String} Returns the hex string of the tag
 * @private
 *
 * ### Example
 *  data        = 47007650A8C9
 *  useful data = 47007650A8
 *  checksum    = C9
 */
function verifyChecksum(data) {
  if (data.length !== 6) {
    throw 'wrong data length';
  }
  const byteArray = new Uint8Array(data);
  const content = byteArray.slice(0, byteArray.length - 1);
  const checkbyte = byteArray[byteArray.length - 1];
  const checksum = content.reduce((acc, val) => acc ^ val, 0);
  if (checksum !== checkbyte) {
    throw 'checksum mismatch';
  }
  return data.slice(0, data.length - 1).toString('hex');
}
