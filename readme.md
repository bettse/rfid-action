# RFID Action

Using a ID-12LA RFID reader for em4100 tags (125KHz), it parses the tag's id and attempts to execute a command defined in either comfig.json or `process.env`

* https://www.sparkfun.com/products/9875

## Install
 * `npm install`

## Run
 * `SERIAL_PORT=/dev/cu.usbserial-... node index`


